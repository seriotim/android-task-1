package com.treelineinteractive.recruitmenttask.data.repository

import com.treelineinteractive.recruitmenttask.data.network.model.ProductItem
import com.treelineinteractive.recruitmenttask.data.network.service.ServiceFactory
import com.treelineinteractive.recruitmenttask.data.network.service.ShopService

class ShopRepository {
    private val shopService = ServiceFactory.createService<ShopService>()
    private val productsLocalCache = ProductsLocalCache()

    suspend fun getProducts(): List<ProductItem> {
        val products = shopService.getInventory()
        productsLocalCache.saveProducts(products)
        return products
    }
}

class ProductsLocalCache {
    private val savedProducts = mutableListOf<ProductItem>()

    fun saveProducts(products: List<ProductItem>) {
    }

    fun getProducts() = savedProducts
}